# Summary

Date : 2021-10-04 14:04:03

Directory j:\nodecg\bundles\asaria

Total : 32 files,  12527 codes, 147 comments, 687 blanks, all 13361 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| CSS | 3 | 7,071 | 4 | 442 | 7,517 |
| JSON | 2 | 3,185 | 0 | 2 | 3,187 |
| JavaScript | 13 | 1,298 | 98 | 158 | 1,554 |
| HTML | 12 | 916 | 24 | 77 | 1,017 |
| SCSS | 1 | 55 | 21 | 6 | 82 |
| Markdown | 1 | 2 | 0 | 2 | 4 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 32 | 12,527 | 147 | 687 | 13,361 |
| dashboard | 21 | 8,258 | 80 | 574 | 8,912 |
| dashboard\css | 1 | 51 | 1 | 11 | 63 |
| dashboard\dialog | 2 | 58 | 0 | 3 | 61 |
| dashboard\js | 1 | 251 | 29 | 31 | 311 |
| dashboard\panels | 13 | 7,452 | 5 | 478 | 7,935 |
| dashboard\panels\alliances | 2 | 57 | 0 | 4 | 61 |
| dashboard\panels\alliances-points | 2 | 120 | 2 | 11 | 133 |
| dashboard\panels\css | 1 | 6,997 | 3 | 425 | 7,425 |
| dashboard\panels\manage-points | 2 | 199 | 0 | 23 | 222 |
| dashboard\panels\test-alerts | 2 | 31 | 0 | 8 | 39 |
| dashboard\panels\twitch | 2 | 34 | 0 | 5 | 39 |
| dashboard\panels\user-lookup | 2 | 14 | 0 | 2 | 16 |
| dashboard\sass | 1 | 55 | 21 | 6 | 82 |
| extension | 6 | 794 | 67 | 87 | 948 |
| extension\db | 1 | 225 | 1 | 11 | 237 |
| extension\lib | 4 | 200 | 17 | 21 | 238 |
| extension\lib\streamlabs | 1 | 60 | 13 | 5 | 78 |
| extension\lib\twitch | 1 | 114 | 2 | 9 | 125 |
| graphics | 2 | 288 | 0 | 22 | 310 |

[details](details.md)