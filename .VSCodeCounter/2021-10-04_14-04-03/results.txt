Date : 2021-10-04 14:04:03
Directory : j:\nodecg\bundles\asaria
Total : 32 files,  12527 codes, 147 comments, 687 blanks, all 13361 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| CSS        |          3 |      7,071 |          4 |        442 |      7,517 |
| JSON       |          2 |      3,185 |          0 |          2 |      3,187 |
| JavaScript |         13 |      1,298 |         98 |        158 |      1,554 |
| HTML       |         12 |        916 |         24 |         77 |      1,017 |
| SCSS       |          1 |         55 |         21 |          6 |         82 |
| Markdown   |          1 |          2 |          0 |          2 |          4 |
+------------+------------+------------+------------+------------+------------+

Directories
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                             | files      | code       | comment    | blank      | total      |
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                |         32 |     12,527 |        147 |        687 |     13,361 |
| dashboard                                                                        |         21 |      8,258 |         80 |        574 |      8,912 |
| dashboard\css                                                                    |          1 |         51 |          1 |         11 |         63 |
| dashboard\dialog                                                                 |          2 |         58 |          0 |          3 |         61 |
| dashboard\js                                                                     |          1 |        251 |         29 |         31 |        311 |
| dashboard\panels                                                                 |         13 |      7,452 |          5 |        478 |      7,935 |
| dashboard\panels\alliances                                                       |          2 |         57 |          0 |          4 |         61 |
| dashboard\panels\alliances-points                                                |          2 |        120 |          2 |         11 |        133 |
| dashboard\panels\css                                                             |          1 |      6,997 |          3 |        425 |      7,425 |
| dashboard\panels\manage-points                                                   |          2 |        199 |          0 |         23 |        222 |
| dashboard\panels\test-alerts                                                     |          2 |         31 |          0 |          8 |         39 |
| dashboard\panels\twitch                                                          |          2 |         34 |          0 |          5 |         39 |
| dashboard\panels\user-lookup                                                     |          2 |         14 |          0 |          2 |         16 |
| dashboard\sass                                                                   |          1 |         55 |         21 |          6 |         82 |
| extension                                                                        |          6 |        794 |         67 |         87 |        948 |
| extension\db                                                                     |          1 |        225 |          1 |         11 |        237 |
| extension\lib                                                                    |          4 |        200 |         17 |         21 |        238 |
| extension\lib\streamlabs                                                         |          1 |         60 |         13 |          5 |         78 |
| extension\lib\twitch                                                             |          1 |        114 |          2 |          9 |        125 |
| graphics                                                                         |          2 |        288 |          0 |         22 |        310 |
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                                         | language   | code       | comment    | blank      | total      |
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| j:\nodecg\bundles\asaria\README.md                                               | Markdown   |          2 |          0 |          2 |          4 |
| j:\nodecg\bundles\asaria\dashboard\asaria-alliances.html                         | HTML       |        223 |          0 |          4 |        227 |
| j:\nodecg\bundles\asaria\dashboard\asaria-debug.html                             | HTML       |        145 |         24 |         35 |        204 |
| j:\nodecg\bundles\asaria\dashboard\css\mystyles.css                              | CSS        |         51 |          1 |         11 |         63 |
| j:\nodecg\bundles\asaria\dashboard\dashboard.css                                 | CSS        |         23 |          0 |          6 |         29 |
| j:\nodecg\bundles\asaria\dashboard\dialog\teampointsreset-dialog.html            | HTML       |         27 |          0 |          1 |         28 |
| j:\nodecg\bundles\asaria\dashboard\dialog\userpointsreset-dialog.html            | HTML       |         31 |          0 |          2 |         33 |
| j:\nodecg\bundles\asaria\dashboard\js\alliances.js                               | JavaScript |        251 |         29 |         31 |        311 |
| j:\nodecg\bundles\asaria\dashboard\panels\alliances-points\alliances-points.html | HTML       |         45 |          0 |          3 |         48 |
| j:\nodecg\bundles\asaria\dashboard\panels\alliances-points\alliances-points.js   | JavaScript |         75 |          2 |          8 |         85 |
| j:\nodecg\bundles\asaria\dashboard\panels\alliances\alliances.html               | HTML       |         32 |          0 |          1 |         33 |
| j:\nodecg\bundles\asaria\dashboard\panels\alliances\alliances.js                 | JavaScript |         25 |          0 |          3 |         28 |
| j:\nodecg\bundles\asaria\dashboard\panels\css\panelcss.css                       | CSS        |      6,997 |          3 |        425 |      7,425 |
| j:\nodecg\bundles\asaria\dashboard\panels\manage-points\manage-points.html       | HTML       |         79 |          0 |          3 |         82 |
| j:\nodecg\bundles\asaria\dashboard\panels\manage-points\manage-points.js         | JavaScript |        120 |          0 |         20 |        140 |
| j:\nodecg\bundles\asaria\dashboard\panels\test-alerts\test-alerts.html           | HTML       |         16 |          0 |          3 |         19 |
| j:\nodecg\bundles\asaria\dashboard\panels\test-alerts\test-alerts.js             | JavaScript |         15 |          0 |          5 |         20 |
| j:\nodecg\bundles\asaria\dashboard\panels\twitch\twitch.html                     | HTML       |         16 |          0 |          2 |         18 |
| j:\nodecg\bundles\asaria\dashboard\panels\twitch\twitch.js                       | JavaScript |         18 |          0 |          3 |         21 |
| j:\nodecg\bundles\asaria\dashboard\panels\user-lookup\user-lookup.html           | HTML       |         14 |          0 |          1 |         15 |
| j:\nodecg\bundles\asaria\dashboard\panels\user-lookup\user-lookup.js             | JavaScript |          0 |          0 |          1 |          1 |
| j:\nodecg\bundles\asaria\dashboard\sass\mystyles.scss                            | SCSS       |         55 |         21 |          6 |         82 |
| j:\nodecg\bundles\asaria\extension\db\db.js                                      | JavaScript |        225 |          1 |         11 |        237 |
| j:\nodecg\bundles\asaria\extension\index.js                                      | JavaScript |        369 |         49 |         55 |        473 |
| j:\nodecg\bundles\asaria\extension\lib\debug.js                                  | JavaScript |         16 |          2 |          5 |         23 |
| j:\nodecg\bundles\asaria\extension\lib\nodecg.js                                 | JavaScript |         10 |          0 |          2 |         12 |
| j:\nodecg\bundles\asaria\extension\lib\streamlabs\api.js                         | JavaScript |         60 |         13 |          5 |         78 |
| j:\nodecg\bundles\asaria\extension\lib\twitch\client.js                          | JavaScript |        114 |          2 |          9 |        125 |
| j:\nodecg\bundles\asaria\graphics\index.html                                     | HTML       |        144 |          0 |         11 |        155 |
| j:\nodecg\bundles\asaria\graphics\index2.html                                    | HTML       |        144 |          0 |         11 |        155 |
| j:\nodecg\bundles\asaria\package-lock.json                                       | JSON       |      3,062 |          0 |          1 |      3,063 |
| j:\nodecg\bundles\asaria\package.json                                            | JSON       |        123 |          0 |          1 |        124 |
| Total                                                                            |            |     12,527 |        147 |        687 |     13,361 |
+----------------------------------------------------------------------------------+------------+------------+------------+------------+------------+