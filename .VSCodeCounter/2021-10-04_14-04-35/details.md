# Details

Date : 2021-10-04 14:04:35

Directory j:\nodecg\bundles\asaria

Total : 32 files,  12527 codes, 147 comments, 687 blanks, all 13361 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 2 | 0 | 2 | 4 |
| [dashboard/asaria-alliances.html](/dashboard/asaria-alliances.html) | HTML | 223 | 0 | 4 | 227 |
| [dashboard/asaria-debug.html](/dashboard/asaria-debug.html) | HTML | 145 | 24 | 35 | 204 |
| [dashboard/css/mystyles.css](/dashboard/css/mystyles.css) | CSS | 51 | 1 | 11 | 63 |
| [dashboard/dashboard.css](/dashboard/dashboard.css) | CSS | 23 | 0 | 6 | 29 |
| [dashboard/dialog/teampointsreset-dialog.html](/dashboard/dialog/teampointsreset-dialog.html) | HTML | 27 | 0 | 1 | 28 |
| [dashboard/dialog/userpointsreset-dialog.html](/dashboard/dialog/userpointsreset-dialog.html) | HTML | 31 | 0 | 2 | 33 |
| [dashboard/js/alliances.js](/dashboard/js/alliances.js) | JavaScript | 251 | 29 | 31 | 311 |
| [dashboard/panels/alliances-points/alliances-points.html](/dashboard/panels/alliances-points/alliances-points.html) | HTML | 45 | 0 | 3 | 48 |
| [dashboard/panels/alliances-points/alliances-points.js](/dashboard/panels/alliances-points/alliances-points.js) | JavaScript | 75 | 2 | 8 | 85 |
| [dashboard/panels/alliances/alliances.html](/dashboard/panels/alliances/alliances.html) | HTML | 32 | 0 | 1 | 33 |
| [dashboard/panels/alliances/alliances.js](/dashboard/panels/alliances/alliances.js) | JavaScript | 25 | 0 | 3 | 28 |
| [dashboard/panels/css/panelcss.css](/dashboard/panels/css/panelcss.css) | CSS | 6,997 | 3 | 425 | 7,425 |
| [dashboard/panels/manage-points/manage-points.html](/dashboard/panels/manage-points/manage-points.html) | HTML | 79 | 0 | 3 | 82 |
| [dashboard/panels/manage-points/manage-points.js](/dashboard/panels/manage-points/manage-points.js) | JavaScript | 120 | 0 | 20 | 140 |
| [dashboard/panels/test-alerts/test-alerts.html](/dashboard/panels/test-alerts/test-alerts.html) | HTML | 16 | 0 | 3 | 19 |
| [dashboard/panels/test-alerts/test-alerts.js](/dashboard/panels/test-alerts/test-alerts.js) | JavaScript | 15 | 0 | 5 | 20 |
| [dashboard/panels/twitch/twitch.html](/dashboard/panels/twitch/twitch.html) | HTML | 16 | 0 | 2 | 18 |
| [dashboard/panels/twitch/twitch.js](/dashboard/panels/twitch/twitch.js) | JavaScript | 18 | 0 | 3 | 21 |
| [dashboard/panels/user-lookup/user-lookup.html](/dashboard/panels/user-lookup/user-lookup.html) | HTML | 14 | 0 | 1 | 15 |
| [dashboard/panels/user-lookup/user-lookup.js](/dashboard/panels/user-lookup/user-lookup.js) | JavaScript | 0 | 0 | 1 | 1 |
| [dashboard/sass/mystyles.scss](/dashboard/sass/mystyles.scss) | SCSS | 55 | 21 | 6 | 82 |
| [extension/db/db.js](/extension/db/db.js) | JavaScript | 225 | 1 | 11 | 237 |
| [extension/index.js](/extension/index.js) | JavaScript | 369 | 49 | 55 | 473 |
| [extension/lib/debug.js](/extension/lib/debug.js) | JavaScript | 16 | 2 | 5 | 23 |
| [extension/lib/nodecg.js](/extension/lib/nodecg.js) | JavaScript | 10 | 0 | 2 | 12 |
| [extension/lib/streamlabs/api.js](/extension/lib/streamlabs/api.js) | JavaScript | 60 | 13 | 5 | 78 |
| [extension/lib/twitch/client.js](/extension/lib/twitch/client.js) | JavaScript | 114 | 2 | 9 | 125 |
| [graphics/index.html](/graphics/index.html) | HTML | 144 | 0 | 11 | 155 |
| [graphics/index2.html](/graphics/index2.html) | HTML | 144 | 0 | 11 | 155 |
| [package-lock.json](/package-lock.json) | JSON | 3,062 | 0 | 1 | 3,063 |
| [package.json](/package.json) | JSON | 123 | 0 | 1 | 124 |

[summary](results.md)