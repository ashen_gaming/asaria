const connectionStatus = nodecg.Replicant("connectionStatus");
const connectedChannel = nodecg.Replicant("connectedChannel");
const statusText = document.getElementById("statusText");
const connectedChannelText = document.getElementById("connectedChannelText");

connectionStatus.on("change", (newValue, oldValue) => {
	console.log(`Twitch connection status changed!`, false);
	console.log(newValue)
	if(newValue === "Connected"){
		statusText.innerHTML = "<i class='fas fa-heartbeat' style='color: green'></i> " + newValue;
	}else{
		statusText.innerHTML = "<i class='fas fa-heartbeat' style='color: red'></i> " + newValue;
	}
});

connectedChannel.on("change", (newValue, oldValue) => {
	console.log(`Twitch channel changed!`, false);
	console.log(newValue)
	connectedChannelText.innerHTML =  newValue;
});
