var latestDonation = nodecg.Replicant("latestDonation");
var latestSubscription = nodecg.Replicant("latestSubscription");
var latestCheer = nodecg.Replicant("latestCheer");

var testDonationButton = document.getElementById("testDonationButton");
var testSubscriptionButton = document.getElementById("testSubscriptionButton");
var testCheerButton = document.getElementById("testCheerButton");

testDonationButton.addEventListener('click', () => {
	nodecg.sendMessage('testDonation').then(console.log('Test donation sent!'));
})

testSubscriptionButton.addEventListener('click', () => {
	nodecg.sendMessage('testSubscription').then(console.log('Test subscription sent!'));
})

testCheerButton.addEventListener('click', () => {
	nodecg.sendMessage('testCheer').then(console.log('Test cheer sent!'));
})
