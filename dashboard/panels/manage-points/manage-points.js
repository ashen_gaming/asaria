
var repT1Sub = nodecg.Replicant("t1sub");
var repT2Sub = nodecg.Replicant("t2sub");
var repT3Sub = nodecg.Replicant("t3sub");
var repGiftedSub = nodecg.Replicant("giftedsub");
var repSubGifter = nodecg.Replicant("subgifter");
var repTip = nodecg.Replicant("tip");
var repCheer = nodecg.Replicant("cheer");


var t1Sub = document.getElementById("t1Sub");
var t2Sub = document.getElementById("t2Sub");
var t3Sub = document.getElementById("t3Sub");
var giftedSub = document.getElementById("giftedSub");
var subGifter = document.getElementById("subGifter");
var cheer = document.getElementById("cheer");
var tip = document.getElementById("tip");
var setT1Sub = document.getElementById("setT1Sub");
var setT2Sub = document.getElementById("setT2Sub");
var setT3Sub = document.getElementById("setT3Sub");
var setGiftedSub = document.getElementById("setGiftedSub");
var setSubGifter = document.getElementById("setSubGifter");
var setCheer = document.getElementById("setCheer");
var setTip = document.getElementById("setTip");
var setT1SubText = document.getElementById("setT1SubText");
var setT2SubText = document.getElementById("setT2SubText");
var setT3SubText = document.getElementById("setT3SubText");
var setGiftedSubText = document.getElementById("setGiftedSubText");
var setSubGifterText = document.getElementById("setSubGifterText");
var setCheerText = document.getElementById("setCheerText");
var setTipText = document.getElementById("setTipText");

repT1Sub.on("change", (newValue, oldValue) => {
	console.log(`T1 point value changed.`, false);
	console.log(newValue, false);
	t1Sub.value = newValue;
  });

  repT2Sub.on("change", (newValue, oldValue) => {
	console.log(`T2 point value changed.`, false);
	console.log(newValue, false);
	t2Sub.value = newValue;
  });

  repT3Sub.on("change", (newValue, oldValue) => {
	console.log(`T3 point value changed.`, false);
	console.log(newValue, false);
	t3Sub.value = newValue;
  });

  repGiftedSub.on("change", (newValue, oldValue) => {
	console.log(`Gifted point value changed.`, false);
	console.log(newValue, false);
	giftedSub.value = newValue;
  });

  repSubGifter.on("change", (newValue, oldValue) => {
	console.log(`Gifted point value changed.`, false);
	console.log(newValue, false);
	subGifter.value = newValue;
  });

  repCheer.on("change", (newValue, oldValue) => {
	console.log(`Cheer point value changed.`, false);
	console.log(newValue, false);
	cheer.value = newValue;
  });

  repTip.on("change", (newValue, oldValue) => {
	console.log(`Tip point value changed.`, false);
	console.log(newValue, false);
	tip.value = newValue;
  });

  setT1Sub.addEventListener("click", function () {
	updatePointsValue("t1");
  });

  setT2Sub.addEventListener("click", function () {
	updatePointsValue("t2");
  });

  setT3Sub.addEventListener("click", function () {
	updatePointsValue("t3");
  });

  setGiftedSub.addEventListener("click", function () {
	updatePointsValue("gifted");
  });

  setSubGifter.addEventListener("click", function () {
	updatePointsValue("gifter");
  });

  setCheer.addEventListener("click", function () {
	updatePointsValue("cheer");
  });

  setTip.addEventListener("click", function () {
	updatePointsValue("tip");
  });

  function updatePointsValue(event) {
	let val = 0;
	switch (event) {
	  case "t1":
		console.log("T1 Sub Set button clicked!", false);
		console.log(setT1SubText.value, false);
		val = setT1SubText.value;
		break;
	  case "t2":
		val = setT2SubText.value;
		break;
	  case "t3":
		val = setT3SubText.value;
		break;
	  case "gifted":
		val = setGiftedSubText.value;
		break;
	  case "gifter":
		val = setSubGifterText.value;
		break;
	  case "cheer":
		val = setCheerText.value;
		break;
	  case "tip":
		val = setTipText.value;
		break;
	}

	nodecg
	  .sendMessage("updatePoints", { event, val })
	  .then((result) => {
		console.log("Points updated!", false);
	  })
	  .catch((error) => {
		console.error(error);
	  });
  }
