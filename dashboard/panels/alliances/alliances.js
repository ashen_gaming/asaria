var teamPoints = nodecg.Replicant("teamPoints");

teamPoints.on("change", (newValue, oldValue) => {
  console.log(`teamPoints changed from ${oldValue} to ${newValue}`, true);
  console.log(JSON.stringify(newValue), true);
  var eternalflame = document.getElementById("eternalflame");
  var wintersembrace = document.getElementById("wintersembrace");
  var etherealbloom = document.getElementById("etherealbloom");
  var shadowgrove = document.getElementById("shadowgrove");
  $(eternalflame).fadeOut("fast", function () {
    eternalflame.innerHTML = newValue.eternalflame;
    $(eternalflame).fadeIn("fast");
  });
  $(wintersembrace).fadeOut("fast", function () {
    wintersembrace.innerHTML = newValue.wintersembrace;
    $(wintersembrace).fadeIn("fast");
  });
  $(etherealbloom).fadeOut("fast", function () {
    etherealbloom.innerHTML = newValue.etherealbloom;
    $(etherealbloom).fadeIn("fast");
  });
  $(shadowgrove).fadeOut("fast", function () {
    shadowgrove.innerHTML = newValue.shadowgrove;
    $(shadowgrove).fadeIn("fast");
  });
});

