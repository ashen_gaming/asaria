var addButton = document.getElementById("add-button");
var removeButton = document.getElementById("remove-button");
var enableButton = document.getElementById("enable-button");
var disableButton = document.getElementById("disable-button");
var resetPointsButton = document.getElementById("reset-points-button");
var resetTeamPointsButton = document.getElementById("reset-teams-button");
var valueInput = document.getElementById("valueInput");
var teamSelection;
var teamPoints = nodecg.Replicant("teamPoints");

enableButton.disabled = true;
disableButton.disabled = false;

addButton.addEventListener("click", function () {
  teamSelection = document.querySelector('input[name="teamSelection"]:checked').value
  updateTeams("add", teamSelection, valueInput.value);
});
removeButton.addEventListener("click", function () {
  teamSelection = document.querySelector('input[name="teamSelection"]:checked').value
  updateTeams("remove", teamSelection, valueInput.value);
});

enableButton.addEventListener("click", function () {
  enableCounting(true);
  disableButton.disabled = false;
  enableButton.disabled = true;
});

disableButton.addEventListener("click", function () {
  enableCounting(false);
  enableButton.disabled = false;
  disableButton.disabled = true;
});

function enableCounting(boolean) {
  nodecg.sendMessage("enableCounting", boolean);
}

// TODO: Fix this function to work with replicants rather than text content of the points.
function updateTeams(option, selected, amount) {
  console.log(selected);
  nodecg.readReplicant('teamPoints', value => {
	  console.log('Team Points values: ' + JSON.stringify(value[selected]));
	  if (teamSelection.value == "choose") {
		console.log("Choose a team!", false);
	  } else {
		//let selected = teamSelection.value;
		if (option == "add") {
		  let currentPoints = value[selected];
		  let newPoints =
			parseInt(currentPoints, 10) + parseInt(valueInput.value, 10);
		  console.log(parseInt(newPoints, 10), false);
		  let updatedPoints = parseInt(newPoints, 10);
		  nodecg
			.sendMessage("updateCount", { updatedPoints, selected })
			.then((result) => {
			  console.log("Values updated!", false);
			})
			.catch((error) => {
			  console.error(error);
			});
		} else if (option == "remove") {
		  let currentPoints = value[selected];
		  let newPoints =
			parseInt(currentPoints, 10) - parseInt(valueInput.value, 10);
		  console.log(parseInt(newPoints, 10), false);
		  let updatedPoints = parseInt(newPoints, 10);
		  if (updatedPoints <= 0) {
			updatedPoints = 0;
		  }
		  nodecg
			.sendMessage("updateCount", { updatedPoints, selected })
			.then((result) => {
			  console.log("Values updated!", false);
			})
			.catch((error) => {
			  console.error(error);
			});
		} else {
		}
	  }
  })

}
