"use strict";

/* --------------------------------------------
  NODECG - REGISTER REQUIRED LIBRARIES
-------------------------------------------- */

// Import .env file for credentials and other environment variables.
require("dotenv").config();

// Load Controllers for all systems.
var logging = require('./controllers/status/logging');
var core = require('./controllers/core/core');
var db = require('./controllers/db/db');
var integrations = require('./controllers/integrations/integrations');
var connectionStatus = require('./controllers/status/status');

module.exports = async (nodecg) => {
	// Sets up an instance of NodeCG that can be accessed by all other services.
	logging.info("Setting up NodeCG instance...");
	core.setNodeCG(nodecg);

	logging.info("Registering replicants...")

	/* --------------------------------------------
		NODECG - REGISTER REPLICANTS
	  -------------------------------------------- */

	const latestDonation = nodecg.Replicant("latestDonation", {
		defaultValue: 0,
	});
	const latestSubscription = nodecg.Replicant("latestSubscription", {
		defaultValue: 0,
	});
	const latestCheer = nodecg.Replicant("latestCheer", { defaultValue: 0 });
	const teamPoints = nodecg.Replicant("teamPoints", { defaultValue: 0 });
	const t1sub = nodecg.Replicant("t1sub", { defaultValue: 5 });
	const t2sub = nodecg.Replicant("t2sub", { defaultValue: 10 });
	const t3sub = nodecg.Replicant("t3sub", { defaultValue: 15 });
	const giftedsub = nodecg.Replicant("giftedsub", { defaultValue: 1 });
	const subgifter = nodecg.Replicant("subgifter", { defaultValue: 5 });
	const tip = nodecg.Replicant("tip", { defaultValue: 1 });
	const cheer = nodecg.Replicant("cheer", { defaultValue: 1 });
	const enableCounting = nodecg.Replicant("enableCounting", {
		defaultValue: true,
	});
	const connectionStatus = nodecg.Replicant("connectionStatus", {
		defaultValue: "disconnected",
	});
	const connectedChannel = nodecg.Replicant("connectedChannel", {
		defaultValue: "none",
	});

	logging.info("Replicants succesfully registered.")
};
