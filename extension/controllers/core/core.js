"use strict";
var logging = require('../status/logging');
var context;
var _ = {
    getNodeCG: function () {
        return context;
    },
    setNodeCG: function (ctx) {
        context = ctx;
        logging.info('NodeCG instance successfully registered.');
    }
};
module.exports = _;
