var logging = require('../status/logging')

let context;

const _ = {
  getNodeCG() {
    return context;
  },

  setNodeCG(ctx) {
    context = ctx;
	logging.info('NodeCG instance successfully registered.')
  },
};

export = _;
