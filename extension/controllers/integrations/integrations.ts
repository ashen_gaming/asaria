let useStreamlabs = true;
let useStreamelements = false;
let enableAPI = false;
const twitch = require('./twitch/client');
const api = require('./api/api');
const streamlabs = require('./streamlabs/sl')

const _ = {
	useStreamelements,
	useStreamlabs,
	enableAPI,
	twitch,
	api,
	streamlabs
}

export = _;
