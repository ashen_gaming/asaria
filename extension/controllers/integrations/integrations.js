"use strict";
var useStreamlabs = true;
var useStreamelements = false;
var enableAPI = false;
var twitch = require('./twitch/client');
var api = require('./api/api');
var streamlabs = require('./streamlabs/sl');
var _ = {
    useStreamelements: useStreamelements,
    useStreamlabs: useStreamlabs,
    enableAPI: enableAPI,
    twitch: twitch,
    api: api,
    streamlabs: streamlabs
};
module.exports = _;
