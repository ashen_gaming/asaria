"use strict";
var express = require('express');
var api = express();
var port = process.env.API_PORT;
var logging = require('../../status/logging');
logging.info('Loading API...');
api.get('/', function (req, res) {
    res.send('Hello, world!');
});
api.listen(port, function () {
    logging.info('API listening for connections.');
});
module.exports = api;
