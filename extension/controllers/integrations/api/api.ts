const express = require('express')
const api = express()
const port = process.env.API_PORT
var logging = require('../../status/logging')
logging.info('Loading API...')
api.get('/', (req, res) => {
	res.send('Hello, world!')
})

api.listen(port, () => {
	logging.info('API listening for connections.')
})

export = api;

