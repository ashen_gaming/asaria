var logging = require('../../../status/logging');
import { PubSubSubscriptionMessage } from '@twurple/pubsub'

logging.info('Loaded subscription module.');

export = async(chatClient, pubsubClient, userId, apiClient) => {

	await pubsubClient.onSubscription(userId, (message: PubSubSubscriptionMessage) => {

		/** Subscriber Schema - See: https://twurple.js.org/reference/pubsub/classes/PubSubSubscriptionMessage.html */

		logging.info(`${message.userDisplayName} just subscribed.`);
	})
};
