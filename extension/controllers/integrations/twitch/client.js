"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var fs_1 = require("fs");
var getNodeCG = require('../../core/core').getNodeCG;
var _a = require('@twurple/auth'), StaticAuthProvider = _a.StaticAuthProvider, exchangeCode = _a.exchangeCode, RefreshingAuthProvider = _a.RefreshingAuthProvider;
var ApiClient = require('@twurple/api').ApiClient;
var ChatClient = require('@twurple/chat').ChatClient;
var PubSubClient = require('@twurple/pubsub').PubSubClient;
var logging = require('../../status/logging');
var glob = require('glob');
var path = require('path');
var nodecg = getNodeCG();
var twitchClient;
logging.info("Loading Twitch integration...");
logging.info("Setting up Twitch authentication for bot...");
var authProvider = new StaticAuthProvider(process.env.TWITCH_CLIENT_TOKEN, process.env.TWITCH_OAUTH_TOKEN);
logging.info("Setting up an API client...");
var apiClient = new ApiClient({ authProvider: authProvider });
logging.info("Connecting to twitch chat...");
var chatClient = new ChatClient({ authProvider: authProvider, channels: [process.env.TWITCH_STREAMER] });
logging.info("Connecting to Twitch PubSub system...");
var pubsubClient = new PubSubClient();
function init() {
    return __awaiter(this, void 0, void 0, function () {
        var tokenData, _a, _b, clientId, clientSecret, streamerAuthProvider, userId;
        var _this = this;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    logging.info("Setting up Twitch authentication for streamer...");
                    _b = (_a = JSON).parse;
                    return [4 /*yield*/, fs_1.promises.readFile('./bundles/asaria/tokens.json', 'utf-8')];
                case 1:
                    tokenData = _b.apply(_a, [_c.sent()]);
                    clientId = process.env.TWITCH_CLIENT_ID;
                    clientSecret = process.env.TWITCH_CLIENT_SECRET;
                    streamerAuthProvider = new RefreshingAuthProvider({
                        clientId: clientId,
                        clientSecret: clientSecret,
                        onRefresh: function (newTokenData) { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, fs_1.promises.writeFile('./tokens.json', JSON.stringify(newTokenData, null, 4), 'utf-8')];
                                case 1: return [2 /*return*/, _a.sent()];
                            }
                        }); }); }
                    }, tokenData);
                    return [4 /*yield*/, pubsubClient.registerUserListener(streamerAuthProvider)];
                case 2:
                    userId = _c.sent();
                    logging.info('Connected to Twitch PubSub system.');
                    // Short code to load all of the events into the client.
                    glob.sync(__dirname + "\\events\\*.js").forEach(function (element) {
                        logging.debug(element);
                        require(path.resolve(element))(chatClient, pubsubClient, userId, apiClient);
                    });
                    logging.info('All Twitch events modules loaded.');
                    return [4 /*yield*/, chatClient.connect()];
                case 3:
                    _c.sent();
                    chatClient.onRegister(function () {
                        logging.info("Connected to Twitch chat. Channel: " + process.env.TWITCH_STREAMER);
                        logging.info("Twitch integration successfully registered.");
                    });
                    chatClient.onConnect(function () {
                        logging.info("Connected to Twitch chat. Channel: " + process.env.TWITCH_STREAMER);
                    });
                    chatClient.onDisconnect(function (manual, reason) {
                        if (manual) {
                            logging.info("Twitch chat has been disconnected.");
                        }
                        else {
                            logging.error("Twitch chat has been disconnected. Reason: " + reason);
                        }
                    });
                    return [2 /*return*/];
            }
        });
    });
}
function getTwitchUserByID(twitchId) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, apiClient.users.getUserById(twitchId).name];
                case 1: return [2 /*return*/, (_a.sent()) || null];
            }
        });
    });
}
function getTwitchUserByName(username) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, apiClient.users.getUserByName(username).id];
                case 1: return [2 /*return*/, (_a.sent()) || null];
            }
        });
    });
}
init();
var _ = {
    twitchClient: twitchClient,
    getTwitchUserByID: getTwitchUserByID,
    getTwitchUserByName: getTwitchUserByName
};
module.exports = _;
