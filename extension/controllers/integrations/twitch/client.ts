import { promises as fs } from 'fs';

const { getNodeCG } = require('../../core/core')
const { StaticAuthProvider, exchangeCode, RefreshingAuthProvider } = require('@twurple/auth')
const { ApiClient } = require('@twurple/api')
const { ChatClient } = require('@twurple/chat')
const { PubSubClient } = require('@twurple/pubsub')
var logging = require('../../status/logging')

var glob = require('glob')
var path = require('path')

let nodecg = getNodeCG();
let twitchClient;

logging.info("Loading Twitch integration...")

logging.info("Setting up Twitch authentication for bot...")
const authProvider = new StaticAuthProvider(process.env.TWITCH_CLIENT_TOKEN, process.env.TWITCH_OAUTH_TOKEN)

logging.info("Setting up an API client...")
const apiClient = new ApiClient({ authProvider });

logging.info("Connecting to twitch chat...")
const chatClient = new ChatClient({ authProvider, channels: [process.env.TWITCH_STREAMER]});

logging.info("Connecting to Twitch PubSub system...")
const pubsubClient = new PubSubClient();

async function init() {

		logging.info("Setting up Twitch authentication for streamer...")
		const tokenData = JSON.parse(await fs.readFile('./bundles/asaria/tokens.json', 'utf-8'));
		const clientId = process.env.TWITCH_CLIENT_ID;
		const clientSecret = process.env.TWITCH_CLIENT_SECRET;
		/**Only needed for generating initial tokens.
		const code = process.env.TWITCH_STREAMER_CODE;
		const redirect = process.env.TWITCH_REDIRECT_URL;
		const token = await exchangeCode(clientId, clientSecret, code, redirect);
		console.log(token);
		*/
		const streamerAuthProvider = new RefreshingAuthProvider({
			clientId,
			clientSecret,
			onRefresh: async newTokenData => await fs.writeFile('./tokens-twitch.json', JSON.stringify(newTokenData, null, 4), 'utf-8')
		}, tokenData);

		const userId = await pubsubClient.registerUserListener(streamerAuthProvider);
		logging.info('Connected to Twitch PubSub system.')
		// Short code to load all of the events into the client.
		glob.sync(__dirname + "\\events\\*.js").forEach(element => {
			logging.debug(element)
			require(path.resolve(element))(chatClient, pubsubClient, userId, apiClient);
			});
		logging.info('All Twitch events modules loaded.')
	await chatClient.connect();

	chatClient.onRegister(() => {
		logging.info("Connected to Twitch chat. Channel: " + process.env.TWITCH_STREAMER);
		logging.info("Twitch integration successfully registered.")
	})

	chatClient.onConnect(() => {
		logging.info("Connected to Twitch chat. Channel: " + process.env.TWITCH_STREAMER);
	})

	chatClient.onDisconnect((manual, reason) => {
		if (manual) {
			logging.info("Twitch chat has been disconnected.");
		} else {
			logging.error("Twitch chat has been disconnected. Reason: " + reason);
		}
	})
}

async function getTwitchUserByID(twitchId: number) {
	return await apiClient.users.getUserById(twitchId).name || null;
}

async function getTwitchUserByName(username: string) {
	return await apiClient.users.getUserByName(username).id || null;
}

init()

const _ = {
	twitchClient,
	getTwitchUserByID,
	getTwitchUserByName
}

export = _;
