"use strict";
var fs = require('fs');
var path = require('path');
var logging = require('../../status/logging');
var io = require('socket.io-client');
var streamlabs = io("https://sockets.streamlabs.com?token=" + process.env.SL_SOCKET_TOKEN, {
    transports: ['websocket']
});
logging.info('Streamlabs integration successfully registered.');
module.exports = function () {
    streamlabs.on('event', function (eventData) {
        logging.debug(eventData);
        if (eventData.type === 'donation') {
            logging.debug('New donation from ' + eventData.message[0].name + ' - Amount: ' + eventData.message[0].amount);
        }
    });
};
