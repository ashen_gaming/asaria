const fs = require('fs')
const path = require('path')
var logging = require('../../status/logging')

const io = require('socket.io-client')

const streamlabs = io(`https://sockets.streamlabs.com?token=${process.env.SL_SOCKET_TOKEN}`, {
	transports: ['websocket']
});
logging.info('Streamlabs integration successfully registered.')

export = () => {
	streamlabs.on('event', (eventData) => {
		logging.debug(eventData)
		if(eventData.type === 'donation') {
			logging.debug('New donation from ' + eventData.message[0].name + ' - Amount: ' + eventData.message[0].amount)
		}
	})
}
