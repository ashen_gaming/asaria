
function info(input: string) {
	console.log('INFO ' + input);
}

function error(input: string) {
	console.error('%c%s', 'color: #00e600', 'ERROR ' + input);
}

function warning(input: string) {
	console.warn('WARN ' + input);
}

function debug(input: string) {
 	console.debug('%c%s', 'color: #d3d3d3', 'DEBUG ' + input);
}

const _ =  {
	info,
	warning,
	error,
	debug
}

export = _;
