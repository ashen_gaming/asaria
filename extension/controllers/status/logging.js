"use strict";
function info(input) {
    console.log('INFO ' + input);
}
function error(input) {
    console.error('%c%s', 'color: #00e600', 'ERROR ' + input);
}
function warning(input) {
    console.warn('WARN ' + input);
}
function debug(input) {
    console.debug('%c%s', 'color: #d3d3d3', 'DEBUG ' + input);
}
var _ = {
    info: info,
    warning: warning,
    error: error,
    debug: debug
};
module.exports = _;
