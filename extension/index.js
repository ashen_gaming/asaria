"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
/* --------------------------------------------
  NODECG - REGISTER REQUIRED LIBRARIES
-------------------------------------------- */
// Import .env file for credentials and other environment variables.
require("dotenv").config();
// Load Controllers for all systems.
var logging = require('./controllers/status/logging');
var core = require('./controllers/core/core');
var db = require('./controllers/db/db');
var integrations = require('./controllers/integrations/integrations');
var connectionStatus = require('./controllers/status/status');
module.exports = function (nodecg) { return __awaiter(void 0, void 0, void 0, function () {
    var latestDonation, latestSubscription, latestCheer, teamPoints, t1sub, t2sub, t3sub, giftedsub, subgifter, tip, cheer, enableCounting, connectionStatus, connectedChannel;
    return __generator(this, function (_a) {
        // Sets up an instance of NodeCG that can be accessed by all other services.
        logging.info("Setting up NodeCG instance...");
        core.setNodeCG(nodecg);
        logging.info("Registering replicants...");
        latestDonation = nodecg.Replicant("latestDonation", {
            defaultValue: 0
        });
        latestSubscription = nodecg.Replicant("latestSubscription", {
            defaultValue: 0
        });
        latestCheer = nodecg.Replicant("latestCheer", { defaultValue: 0 });
        teamPoints = nodecg.Replicant("teamPoints", { defaultValue: 0 });
        t1sub = nodecg.Replicant("t1sub", { defaultValue: 5 });
        t2sub = nodecg.Replicant("t2sub", { defaultValue: 10 });
        t3sub = nodecg.Replicant("t3sub", { defaultValue: 15 });
        giftedsub = nodecg.Replicant("giftedsub", { defaultValue: 1 });
        subgifter = nodecg.Replicant("subgifter", { defaultValue: 5 });
        tip = nodecg.Replicant("tip", { defaultValue: 1 });
        cheer = nodecg.Replicant("cheer", { defaultValue: 1 });
        enableCounting = nodecg.Replicant("enableCounting", {
            defaultValue: true
        });
        connectionStatus = nodecg.Replicant("connectionStatus", {
            defaultValue: "disconnected"
        });
        connectedChannel = nodecg.Replicant("connectedChannel", {
            defaultValue: "none"
        });
        logging.info("Replicants succesfully registered.");
        return [2 /*return*/];
    });
}); };
